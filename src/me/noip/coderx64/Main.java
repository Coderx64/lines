package me.noip.coderx64;

import java.awt.Dimension;
import javax.swing.JFrame;

import me.noip.coderx64.game.Board;
import me.noip.coderx64.game.Settings;

public class Main {
	
	public static void main(String[] args) {
		
		JFrame mainWindow = new JFrame("Lines");
		mainWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//mainWindow.setLocationRelativeTo(null);
		mainWindow.setResizable(false);
		mainWindow.getContentPane().setPreferredSize(new Dimension(Settings.cellSide * Settings.boardWidth, Settings.cellSide * Settings.boardHeight));
		mainWindow.pack();
		Board board = new Board();
		mainWindow.add(board);
		mainWindow.setVisible(true);
		
		board.startGame();
	}

}

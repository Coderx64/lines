package me.noip.coderx64.game;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.util.Set;

import javax.swing.*;

public class Cell {
	
	private Ball ball;
	private Integer x,y;
	private Integer side = Settings.cellSide;
	private Boolean isActive = false;
	
	public Cell(int x, int y){
		this.x = x;
		this.y = y;
	}
	
	public Cell(int x, int y, Ball ball){
		this.x = x;
		this.y = y;
		this.ball = ball;
	}
	
	public void clicked(Object sender){
		//System.out.println("Clicked " + x + ":" + y + " cell.");
		setIsActive(true);
		paintCell(((Board)sender).getGraphics());
	}
	
	public Ball getBall(){
		return ball;
	}
	
	
	
	public void setBall(Ball ball){
		this.ball = ball;
	}
	
	public void paintCell(Graphics g){
		g = (Graphics2D) g;
		if (getIsActive()){
			g.setColor(Color.darkGray);
		}else{
			g.setColor(Color.lightGray);
		}
		g.fill3DRect(x * side, y * side, side - Settings.gridWidth, side - Settings.gridWidth, true);
		if (this.ball != null){
			this.ball.paintBall(g);
		}
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	
	public int getX(){
		return this.x;
	}
	
	public int getY(){
		return this.y;
	}
	
	public void Dispose(){
		this.ball = null;
		this.isActive = false;
	}
	
}

package me.noip.coderx64.game;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

public class Ball extends Point{
	
	private Integer radius = Settings.ballRadius;
	private Color color;
	private int cellOffset = (Settings.cellSide - Settings.ballRadius)/2;
	
	public Ball(int x, int y, Color clr){
		super(x, y);
		this.setColor(clr);
	}
	
	public void paintBall(Graphics g){
		g.setColor(color);
		g.fillOval(x * Settings.cellSide + cellOffset, y * Settings.cellSide + cellOffset, radius, radius); 
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}
}

package me.noip.coderx64.game;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class Board extends JPanel{
	
	private Cell[][] cells;
	
	private static Board board;
	private Integer boardWidth = Settings.boardWidth;
	private Integer boardHeight = Settings.boardHeight;
	private Cell activeCell;
	private int score = 0;
	private boolean boardModified = false;
	
	public Board(){
		cells = new Cell[boardWidth][boardHeight];
		
		for (int i = 0; i < boardWidth; i++){
			for (int j = 0; j<boardHeight; j++){
				cells[i][j] = new Cell(i,j);
			}
		}
		this.addMouseListener(new EventHandler());
	}
	
	public void startGame(){
		getNextBalls();
		score=0;
	}
	
	public void endGame(){
		activeCell = null;
		for (int i = 0; i < boardWidth; i++){
			for (int j = 0; j<boardHeight; j++){
				cells[i][j] = new Cell(i,j);
			}
		}
		//printBoard(getGraphics());
		getNextBalls();
	}
	
	@Override
	public void paint(Graphics g){
		printBoard(g);
	}
	
	/*public static Board getBoard(){
		if (board == null){
			board = new Board();
		}
		return board;
	}*/
	
	private void getNextBalls(){
		boardModified = false;
		collectBalls();
		if (boardModified){
			return;
		}
		if (!hasNextMove()){
			endGame();
			JOptionPane.showMessageDialog(null, "Game over! Scores: "+score);
			return;
		}
		int i=0;
		while(i < 3){
			int k,j ;
			
			k = new Random().nextInt(boardWidth);
			j = new Random().nextInt(boardHeight);
			
			if (cells[k][j].getBall() == null){
				cells[k][j] = new Cell(k,j,new Ball(k,j,Settings.colors[new Random().nextInt(Settings.colors.length)]));
				cells[k][j].paintCell(getGraphics());
				i++;
			}
		}
		collectBalls();
	}
	
	private void collectBalls(){
		for (int i = 0; i < boardWidth; i++){
			for (int j = 0; j < boardHeight; j++){
				score+=deleteLines(findLines(i, j, 1, 0));
				score+=deleteLines(findLines(i, j, 0, 1));
				score+=deleteLines(findLines(i, j, 1, 1));
				score+=deleteLines(findLines(i, j, 1, -1));
			}
		}
		System.out.println(score);
		printBoard(getGraphics());
	}
	
	private Color getBallColor(int row, int col){
		if (cells[row][col].getBall() != null){
			return cells[row][col].getBall().getColor();
		}else{
			return null;
		}	
	}
	
	private List<Cell> findLines(final int x, final int y, int dx, int dy){
		List<Cell> linesForDeleting = new ArrayList<>();
		Color clr = null;
		if (cells[x][y].getBall() != null) {
			clr = cells[x][y].getBall().getColor();
		} else {
			return Collections.emptyList();
		}
		
		int row = x, col = y;
		
		linesForDeleting.add(cells[row][col]);
		
		row+=dx;
		col+=dy;
		
		while (row >= 0 && col >= 0 && row < boardWidth && col < boardHeight && getBallColor(row, col) == clr){
			linesForDeleting.add(cells[row][col]);
			row+=dx;
			col+=dy;
		}
		
		row = x;
		col = y;
		row-=dx;
		col-=dy;
		
		while (row >= 0 && col >= 0 && row < boardWidth && col < boardHeight && getBallColor(row, col) == clr){
			linesForDeleting.add(cells[row][col]);
			row-=dx;
			col-=dy;
		}
		
		if (linesForDeleting.size()<5){
			linesForDeleting.clear();
		}
		
		return linesForDeleting;
	}
	
	private int deleteLines(List<Cell> items){
		for(Cell item:items){
			item.Dispose();
			boardModified = true;
		}
		return items.size();
	}
	
	private Boolean hasNextMove()
	{
		int used = 0;
		
		for (int i = 0; i < boardWidth; i++){
			for (int j = 0; j < boardHeight; j++){
				if (cells[i][j].getBall() != null){
					used++;
				}
			}
		}
		
		return (boardWidth*boardHeight)-used>3?true:false;
	}
	
	private int[][] getMatrix(){
		
		int[][] tmp = new int[boardWidth][boardHeight];
		
		for (int i = 0; i < boardWidth; i++){
			for (int j = 0; j < boardHeight; j++){
				if (cells[i][j].getBall() != null){
					tmp[i][j] = -1;
				}else{
					tmp[i][j] = 0;
				}
			}
		}
		
		return tmp;
	}
	
	private int[][] getWave(int x, int y, int lvl, int width, int height, int[][] input){
		
		input[x][y] =  lvl;
		
		if (y+1 < height){
			if (input[x][y+1] == 0 || (input[x][y+1] != -1 && input[x][y+1] > lvl)){
				getWave(x, y+1, lvl+1, width, height, input);
			}
		}
		if (x+1 < width){
			if (input[x+1][y] == 0 || (input[x+1][y] != -1 && input[x+1][y] > lvl)){
				getWave(x+1, y, lvl+1, width, height, input);
			}
		}
		if (x-1 >= 0){
			if (input[x-1][y] == 0 || (input[x-1][y] != -1 && input[x-1][y] > lvl)){
				getWave(x-1, y, lvl+1, width, height, input);
			}
		}
		if (y-1 >= 0){
			if (input[x][y-1] == 0 || (input[x][y-1] != -1 && input[x][y-1] > lvl)){
				getWave(x, y-1, lvl+1, width, height, input);
			}
		}
		
		return input;
	}
	
	private Boolean canMove(int startX, int startY, int destX, int destY){
		if (getWave(startX, startY, 1, boardWidth.intValue(), boardHeight.intValue(), getMatrix())[destX][destY]>0){
			return true;
		}else{
			return false;
		}	
	}
	
	private void printBoard(Graphics g){
		for (int i = 0; i < boardWidth; i++){
			for (int j = 0; j<boardHeight; j++){
				cells[i][j].paintCell(g);
			}
		}
	}
	
	private Boolean swapBalls(int x, int y){
		//TODO FindPath
		if (!canMove(activeCell.getX(), activeCell.getY(), x, y)){
			return false;
		}
		
		Ball ball = activeCell.getBall();
		activeCell.Dispose();
		activeCell.paintCell(getGraphics());
		activeCell = cells[x][y];
		activeCell.setBall(new Ball(x,y,ball.getColor()));
		activeCell.setIsActive(true);
		activeCell.paintCell(getGraphics());
		
		return true;
	}
	
	class EventHandler implements MouseListener{
		
		private Integer divider = Settings.cellSide + Settings.gridWidth;

		@Override
		public void mouseClicked(MouseEvent e) {
			// TODO Auto-generated method stub			
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mousePressed(MouseEvent e) {
			if (e.getButton() == 1){
				int row,col;
				row = e.getX()/divider;
				col = e.getY()/divider;
				if (cells[row][col].getBall() != null){
					if (activeCell != null){
						activeCell.setIsActive(false);
						activeCell.paintCell(getGraphics());
					}					
					activeCell = cells[row][col];
					activeCell.clicked(Board.this);
				}else
				{
					if (activeCell != null && activeCell.getIsActive()){
						if (swapBalls(row,col)){
							getNextBalls();
						}
					}
					return;
				}
			}
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}
		
	}
}
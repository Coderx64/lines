package me.noip.coderx64.game;

import java.awt.Color;
public class Settings {

    public static final Color colors[] = { Color.red, Color.blue, Color.yellow,
            Color.cyan, Color.magenta, Color.blue,
            Color.green };
    
    public static final int cellSide = 65;
    public static final int ballRadius = 50;
    public static final int boardWidth = 10;
    public static final int boardHeight = 10;
    public static final int gridWidth = 1;
}